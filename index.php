<?php
require_once __DIR__ . '/vendor/autoload.php';
$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->load();

echo "<h1>Spe Skill Test</h1>";
$date = date('Y-m-d H:i:s');
echo "<pre>Current server datetime (UTC): " . $date . "</pre><br>";

$host = $_ENV['HOST'];
$user = $_ENV['USER'];
$pass = $_ENV['PASS'];
$db = $_ENV['DB'];

$conn = mysqli_connect($host, $user, $pass, $db);

if (mysqli_connect_error()) {
  echo "<pre>Connection failed: " . mysqli_connect_error() . "</pre><br>";
  exit();
} else {
  echo "<pre>Connection successful" . "</pre><br>";
  $sql = "SELECT NOW() AS current_date_time";
  $result = mysqli_query($conn, $sql);

  if ($result) {
    $row = mysqli_fetch_assoc($result);
    echo "<pre>Current MySQL DB datetime (UTC): " . $row['current_date_time'] . "</pre>";
  } else {
    echo "<pre>Query failed: " . mysqli_error($conn) . "</pre>";
  }
}