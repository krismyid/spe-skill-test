FROM composer as builder
COPY . /app
WORKDIR /app
RUN composer install

FROM php:8-fpm
RUN apt-get update && apt-get install nginx default-mysql-client -y
RUN docker-php-ext-install mysqli pdo_mysql

# Copy Nginx configuration
COPY nginx.conf /etc/nginx/sites-enabled/default

# Copy the app code from the builder container
COPY --from=builder /app /var/www/html

# Copy the vendor directory from the builder container
COPY --from=builder /app/vendor /var/www/html/vendor

# Expose port 80
EXPOSE 80

# Start Nginx and PHP-FPM
CMD service nginx start && php-fpm